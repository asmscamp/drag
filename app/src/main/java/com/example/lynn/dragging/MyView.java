package com.example.lynn.dragging;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.example.lynn.dragging.MainActivity.*;

/**
 * Created by lynn on 2/18/2018.
 */

public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        textView = new TextView(context);

        textView.setText("Hello");

        textView.setTextSize(40);

        addView(textView);

        textView.setOnTouchListener(listener);
    }

}
